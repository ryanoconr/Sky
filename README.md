# Sky
##### By Ryan O'Connor

This is a ReactJS weather app built for a job application for BGL.

For this, I am using webpack and webpack-dev-server to run a dev server to test the site while in development and then using just webpack to create a production version of the site which is compiled down to client.min.js.
The dependancies that I am using for this are the following:

+ axios
+ babel-core
+ babel-loader
+ babel-plugin-add-module-exports
+ babel-plugin-react-html-attrs
+ babel-plugin-transform-class-properties
+ babel-plugin-transform-decorators-legacy
+ babel-preset-es2015
+ babel-preset-react
+ babel-preset-stage-0
+ css-loader
+ extract-text-webpack-plugin
+ node-sass
+ postcss-loader
+ react
+ react-dom
+ sass-loader
+ style-loader
+ webpack
+ webpack-dev-server

I have been using SASS for a while now, so when it came to using React I decided to use **node-sass** and **sass-loader** in combination with **css-loader**, **postcss-loader** and **style-loader** to allow me to use SASS with React and Webpack.

I used **axios** for the GET requests as I feel more confident using **axios** rather than the Fetch API and have a lot more experience when it comes to using **axios**. From there I break up the data I receive and take what I need and store those in the state after doing any manipulation that I may have needed to do, like the addition of leading zeroes on times etc.

To run the development server, simply open Terminal and run *npm run dev* in the initial directory. The production version of the site can be created using *npm run prod* instead. The source for the site is in the ***sky*** directory and that is being compiled down using webpack to client.min.js. You can find
a production version of the site running [here](https://weather.ryanoconr.com).
