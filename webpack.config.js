var webpack = require('webpack');
var path = require('path');

const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractSass = new ExtractTextPlugin({
    filename: "[name].[contenthash].css",
});

module.exports = {
  context: path.join(__dirname,"sky"),
  devtool: 'cheap-module-source-map',
  entry: "./js/client.js",
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          {loader: 'style-loader'},
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: "[hash:base64:5]",
            }
          },
          {loader: 'sass-loader'},
        ],
      },
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['react','es2015','stage-0'],
              plugins: ['react-html-attrs','transform-decorators-legacy','transform-class-properties'],
            }
          },
        ]
      },
    ]
  },
  output: {
    path: __dirname+"/sky/",
    filename: "client.min.js"
  },
  plugins: [
    extractSass,
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    })
  ]
};
