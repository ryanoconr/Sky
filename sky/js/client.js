import React from 'react';
import ReactDOM from 'react-dom';

// using axios as i have more experience using axios rather than the fetch api
import axios from 'axios';

// importing style from scss files here
import style from './style.scss';

class SidebarItem extends React.Component {
  constructor(props){
    super(props);
    var selected = false;
    // check if this city is the selected city
    if (props.featured === props.city){
      selected = true;
    }
    this.state = {
      bg: null,
      city: null,
      fullcity: null,
      sunrise: null,
      sunset: null,
      icon: null,
      temp: null,
      selected,
    };
  }
  componentWillReceiveProps(nextProps){
    var selected = false;
    // check if this city is the new selected city
    if (nextProps.featured === nextProps.city){
      selected = true;
    }
    this.setState({selected});
  }
  componentWillMount(){
    const city = this.props.city;
    // get data from api adding city from variable and setting units to metric to ensure celcius rather than kelvin
    axios.get('https://api.openweathermap.org/data/2.5/weather?q='+city+'&units=metric&appid=bcaf9442c3d86b2d6d5aceb2181dc8b1')
    .then(res => {
      // these are the codes given by the api for different weather conditions
      var codes = ["01d","01n","02d","02n","03d","03n","04d","04n","09d","09n","10d","10n","11d","11n","13d","13n","50d","50n"];
      // these are the corresponding icons
      var icons = ["day-sunny","night-clear","day-cloudy","night-alt-cloudy","cloud","cloud","cloudy","cloudy","showers","showers","day-showers","night-alt-showers","thunderstorm","thunderstorm","snow","snow","fog","fog"];
      // select icon by selecting the value from the array that has the same index as the codes array
      var icon = "wi wi-"+icons[codes.indexOf(res.data.weather[0].icon)];

      // create formatted date from unix timestamp after correcting year
      var sunrise = new Date(res.data.sys.sunrise*1000);
      // get hours for sunrise time
      var sunrise_hours = sunrise.getHours();
      // fix issue of potentially missing leading zero on the hour by adding a zero and then removing everything except the last two characters
      sunrise_hours = ("0"+sunrise_hours).slice(-2);
      // get minutes for sunrise time
      var sunrise_minutes = sunrise.getMinutes();
      // fix issue of potentially missing leading zero on the minutes by adding a zero and then removing everything except the last two characters
      sunrise_minutes = ("0"+sunrise_minutes).slice(-2);
      // put hours and minutes together for a formatted sunrise time
      sunrise = sunrise_hours+":"+sunrise_minutes;

      // create formatted date from unix timestamp after correcting year
      var sunset = new Date(res.data.sys.sunset*1000);
      // get hours for sunset time
      var sunset_hours = sunset.getHours();
      // fix issue of potentially missing leading zero on the hour by adding a zero and then removing everything except the last two characters
      sunset_hours = ("0"+sunset_hours).slice(-2);
      // get minutes for sunset time
      var sunset_minutes = sunset.getMinutes();
      // fix issue of potentially missing leading zero on the minutes by adding a zero and then removing everything except the last two characters
      sunset_minutes = ("0"+sunset_minutes).slice(-2);
      // put hours and minutes together for a formatted sunset time
      sunset = sunset_hours+":"+sunset_minutes;

      // get current temperature
      var temp = Math.round(res.data.main.temp);

      this.setState({
        sunrise,
        sunset,
        icon,
        temp,
      });
    });
  }
  change(){
    // function to pass city new city to parent
    var city = this.props.city;
    this.props.click(city);
  }
  render(){
    return(
      <article className={this.props.bg} onClick={() => this.change()}>
        <span className={this.state.selected ? style.selected : null}>
          <h1>{this.props.city}</h1>
          <h3><div><i class={this.state.icon}></i>{this.state.temp}&deg;</div></h3>
          <h2><i class="wi wi-sunrise"></i>{this.state.sunrise}</h2>
          <h2><i class="wi wi-sunset"></i>{this.state.sunset}</h2>
        </span>
      </article>
    )
  }
}

class Featured extends React.Component {
  constructor(){
    super();
    this.setBackground = this.setBackground.bind(this);
    this.fetchData = this.fetchData.bind(this);
    this.changeCity = this.changeCity.bind(this);
    this.state = {
      bg: null,
      day1: null,
      day2: null,
      day3: null,
      day4: null,
      day5: null,
    }
  }
  // function to change the background depending on the selected city
  setBackground(city = this.props.city){
    var styles = [style.london,style.paris,style.berlin,style.dublin,style.madrid];
    var cities = ["London","Paris","Berlin","Dublin","Madrid"];
    var bg = styles[cities.indexOf(city)];
    this.setState({
      bg,
    });
  }
  // function to fetch data for selected city
  fetchData(city = this.props.city){
    axios.get('https://api.openweathermap.org/data/2.5/forecast?q='+city+'&units=metric&appid=bcaf9442c3d86b2d6d5aceb2181dc8b1')
    .then(res => {
      // get new date to select forecast at 9am
      var cd = new Date();
      var ch = cd.getHours();
      var diff = null;
      if (ch < 9){
        // if current hour is before 9am, the difference is 9am - currenthour
        diff = 9 - ch;
      } else {
        // if current hour is after 9am, the difference is 9am + 24 hours - currenthour
        diff = 33 - ch;
      }
      var offset = null;
      if (diff <= 3){
        // if the difference is 3 hours or less, the offset is 0
        offset = 0;
      } else {
        // if the difference is more than 3 hours, the offset will be the difference / 3 rounded up then - 1
        offset = Math.ceil(diff/3) - 1;
      }
      // add 8 to the offset for each additional day
      var day1 = res.data.list[offset];
      var day2 = res.data.list[offset+(8*1)];
      var day3 = res.data.list[offset+(8*2)];
      var day4 = res.data.list[offset+(8*3)];
      var day5 = res.data.list[offset+(8*4)];
      this.setState({
        day1,
        day2,
        day3,
        day4,
        day5,
      });
    });
  }
  componentWillMount(){
    // set background and fetch data when component is about to mount
    this.setBackground();
    this.fetchData();
  }
  componentWillReceiveProps(nextProps){
    // set background and fetch data when new props are received
    this.setBackground(nextProps.city);
    this.fetchData(nextProps.city);
  }
  // function to get day of the week
  getDay(num){
    // get current date and day
    var today = new Date();
    var day = today.getDay();
    // day + how many days in the future it is meant to be
    day+=num;
    if (day > 6){
      // if day is greater than 6, -7 to keep within 0-6 options
      day -= 7;
    }
    var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
    return days[day];
  }
  // change city function for mobile view
  changeCity(){
    // set background and fetch data for new city
    this.setBackground(event.target.value);
    this.fetchData(event.target.value);
  }
  render(){
    return(
      <section className={style.featured}>
        <article className={this.state.bg}></article>
        <h5>{this.props.city}</h5>
        {/* dropdown is for mobile view of site when sidebar is hidden */}
        <div className={style.select}>
          <select onChange={this.changeCity}>
            <option value="London" selected>London</option>
            <option value="Paris">Paris</option>
            <option value="Berlin">Berlin</option>
            <option value="Dublin">Dublin</option>
            <option value="Madrid">Madrid</option>
          </select>
          <i class="fa fa-angle-down"></i>
        </div>
        <div className={style.content}>
          <FeatureDay content={this.state.day1} day={this.getDay(1)}/>
          <FeatureDay content={this.state.day2} day={this.getDay(2)}/>
          <FeatureDay content={this.state.day3} day={this.getDay(3)}/>
          <FeatureDay content={this.state.day4} day={this.getDay(4)}/>
          <FeatureDay content={this.state.day5} day={this.getDay(5)}/>
        </div>
      </section>
    );
  }
}

class FeatureDay extends React.Component {
  constructor(){
    super();
    this.state = {
      data: [],
    }
  }
  // this will find closest number in an array to a specified target
  closest(num,arr){
    var curr = arr[0];
    var diff = Math.abs(num-curr);
    for (var val = 0; val < arr.length; val++){
      var newdiff = Math.abs(num - arr[val]);
      if (newdiff < diff){
        diff = newdiff;
        curr = arr[val];
      }
    }
    return curr;
  }
  componentWillReceiveProps(nextProps){
    const data = nextProps.content;

    // same process of selecting weather icon as before
    var codes = ["01d","01n","02d","02n","03d","03n","04d","04n","09d","09n","10d","10n","11d","11n","13d","13n","50d","50n"];
    var icons = ["day-sunny","night-clear","day-cloudy","night-alt-cloudy","cloud","cloud","cloudy","cloudy","showers","showers","day-showers","night-alt-showers","thunderstorm","thunderstorm","snow","snow","fog","fog"];
    var icon = "wi wi-"+icons[codes.indexOf(data.weather[0].icon)];

    var sea_level = data.main.sea_level;
    // rounding the values of temperature, min temperature and max temperaturs
    var temp = Math.round(data.main.temp);
    var temp_min = Math.round(data.main.temp_min);
    var temp_max = Math.round(data.main.temp_max);
    var humidity = data.main.humidity;
    var windspeed = data.wind.speed;
    // round the value of wind direction
    var winddirection = Math.round(data.wind.deg);
    // options for wind direction icons
    var degicons = [0,23,45,68,90,113,135,158,180,203,225,248,270,293,313,336];
    // select icon based on which is closest to actual wind direction
    var diricon = "wi wi-wind towards-"+this.closest(winddirection,degicons)+"-deg";

    var time = data.dt_txt;
    // slice time so only hours and minutes are showing from timestamp
    time = time.slice(-8).slice(0,5);
    this.setState({
      icon,
      sea_level,
      temp,
      temp_min,
      temp_max,
      time,
      humidity,
      windspeed,
      winddirection,
      diricon,
    });
  }
  render(){
    return(
      <div className={style.day}>
        <h1>
          {this.props.day}
          <br/>
          {this.state.time}
        </h1>
        <div className={style.icon}>
          <i class={this.state.icon}></i>
        </div>
        <div className={style.temp}>
          <div className={style.current}>{this.state.temp}&deg;</div>
          <div className={style.hl}>
            <div className={style.low}>L {this.state.temp_min}&deg;</div>
            <div className={style.high}>H {this.state.temp_max}&deg;</div>
          </div>
        </div>
        <h1><i class="wi wi-strong-wind"></i>{this.state.windspeed} m/s</h1>
        <h1><i class={this.state.diricon}></i>{this.state.winddirection}&deg;</h1>
        <h1><i class="wi wi-humidity"></i>{this.state.humidity}%</h1>
        <h1><i class="wi wi-flood"></i>{this.state.sea_level} hPa</h1>
      </div>
    );
  }
}

class MainView extends React.Component {
  // set default city to London
  constructor(){
    super();
    this.state = {
      featured: 'London',
    };
  }
  // this will be called by children when changing the city in desktop view
  changeFeatured(city){
    this.setState({
      featured: city,
    });
  }
  render(){
    return(
      <div>
        <Featured city={this.state.featured}/>
        <section className={style.sidebar}>
          <SidebarItem click={this.changeFeatured.bind(this)} featured={this.state.featured} city="London" bg={style.london}/>
          <SidebarItem click={this.changeFeatured.bind(this)} featured={this.state.featured} city="Paris" bg={style.paris}/>
          <SidebarItem click={this.changeFeatured.bind(this)} featured={this.state.featured} city="Berlin" bg={style.berlin}/>
          <SidebarItem click={this.changeFeatured.bind(this)} featured={this.state.featured} city="Dublin" bg={style.dublin}/>
          <SidebarItem click={this.changeFeatured.bind(this)} featured={this.state.featured} city="Madrid" bg={style.madrid}/>
        </section>
      </div>
    );
  }
}

const app = document.getElementById("appcontainer");
ReactDOM.render(<MainView/>,app);
